void mx_printchar(char c);
void mx_forprint_s(int begin, int end, char c);

static void part0(int row, int half, int twice) {
    if (row == 0) {
        mx_forprint_s(0, half + 1, ' ');
        mx_printchar('+');
        mx_forprint_s(0, twice, '-');
        mx_printchar('+');
    } else if (row < half + 1) {
        mx_forprint_s(0, 1 + half - row, ' ');
        mx_printchar('/');
        mx_forprint_s(0, twice, ' ');
        mx_printchar('/');
        mx_forprint_s(0, row - 1, ' ');
        mx_printchar('|');
    } else if (row == half + 1) {
        mx_printchar('+');
        mx_forprint_s(0, twice, '-');
        mx_printchar('+');
        mx_forprint_s(0, half, ' ');
        mx_printchar('|');
    }
}

static void part1(int row, int half, int twice, int lines) {
    if (row < lines - half - 1) {
        mx_printchar('|');
        mx_forprint_s(0, twice, ' ');
        mx_printchar('|');
        mx_forprint_s(0, half, ' ');
        mx_printchar(row == lines - half - 2 ? '+' : '|');
    } else if (row == lines - 1) {
        mx_printchar('+');
        mx_forprint_s(0, twice, '-');
        mx_printchar('+');
    } else {
        mx_printchar('|');
        mx_forprint_s(0, twice, ' ');
        mx_printchar('|');
        mx_forprint_s(0, -row + lines - 2, ' ');
        mx_printchar('/');
    }
}

void mx_cube(int n) {
    int half = n / 2;
    int twice = n * 2;
    int lines = n + half + 3;

    if (n <= 1) {
        return;
    }

    for (int row = 0; row < lines; ++row) {
        if (row <= half + 1) {
            part0(row, half, twice);
        } else {
            part1(row, half, twice, lines);
        }

        mx_printchar('\n');
    }
}
