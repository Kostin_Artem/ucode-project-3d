void mx_printchar(char c);
void mx_forprint_s(int begin, int end, char c);

static void segment0(int n) {
    mx_forprint_s(0, n - 1, ' ');
    mx_printchar('/');
    mx_printchar('\\');
}

static void segment1(int n, int row) {
    mx_forprint_s(0, n - row - 1, ' ');
    mx_printchar('/');
    mx_forprint_s(0, row * 2 - 1, ' ');
    mx_printchar('\\');
    mx_forprint_s(0, row, ' ');
    mx_printchar('\\');
}

static void segment2(int n, int row) {
    mx_forprint_s(0, n - row - 1, ' ');
    mx_printchar('/');
    mx_forprint_s(0, row * 2 - 1, row == n - 1 ? '_' : ' ');
    mx_printchar('\\');
    mx_forprint_s(0, n - row - 1, ' ');
    mx_printchar('|');
}

void mx_pyramid(int n) {
    int half = n / 2;

    if ((n & 1) == 1 || n <= 1) {
        return;
    }
    
    for (int row = 0; row < n; ++row) {
        if (row == 0) {
            segment0(n);
        } else if (row < half) {
            segment1(n, row);
        } else if (row < n) {
            segment2(n, row);
        }

        mx_printchar('\n');
    }
}
