#include <unistd.h>

void mx_printchar(char c) {
    write(1, &c, 1);
}

void mx_forprint_s(int begin, int end, char c) {
    for (int i = begin; i < end; ++i) {
        mx_printchar(c);
    }
}
